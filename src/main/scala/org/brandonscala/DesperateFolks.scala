package org.brandonscala

import org.openjdk.jmh.annotations
import org.openjdk.jmh.annotations.Scope

import scala.util.Random

/**
  * Desperate folks who continue to post jobs to the list
  */
@annotations.State(Scope.Benchmark)
class DesperateFolks(s: Server) extends Thread{
  private val server = s
  private val random = new Random()

  /**
    * Run method that runs the desperate folks
    */
  override def run(): Unit = {
    var counter: Int = 0
    while(counter < 500) {
      counter += 1
      server.add()
      Thread.sleep(random.nextInt(1000))
    }
  }
}
