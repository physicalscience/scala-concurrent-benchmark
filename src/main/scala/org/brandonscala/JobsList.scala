package org.brandonscala

import org.openjdk.jmh.annotations
import org.openjdk.jmh.annotations.Scope

/**
  * The joblist that the jobs can be posted and taken from
  */
@annotations.State(Scope.Benchmark)
class JobsList[Entry] {
  private val jobList = new collection.concurrent.TrieMap[Integer, Entry]()

  /**
    * Adds a job to the list
    * @param jobNum Takes in Integer that represents the job number
    * @param entry Takes an entry to be added to the list
    */
  def add(jobNum: Integer, entry: Entry): Unit = {
    jobList.put(jobNum, entry)
  }

  /**
    * Grabs an entry from the list
    * @param index The index of the entry to be grabbed
    * @return returns an Option of entry
    */
  def grab(index: Int): Option[Entry] = {
    jobList.get(index) match {
      case Some(o) => Some(o)
      case None => None
    }
  }

  /**
    * Deletes a job from the list
    * @param index the index of the job to be deleted
    * @return returns a boolean to let the caller know if deletion was succesful or not
    */
  def delete(index: Int): Boolean = {
    jobList.remove(index) match {
      case Some(i) => true
      case None => false
    }
  }

  /**
    * grabs the entire joblist
    * @return returns a list of jobs
    */
  def examine() = jobList.toList

  /**
    * Gets the size of the list
    * @return returns the list size
    */
  def size() = {
    jobList.toList.size
  }
}
