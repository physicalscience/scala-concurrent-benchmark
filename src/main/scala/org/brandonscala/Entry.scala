package org.brandonscala

import java.util.concurrent.atomic.AtomicReference

import org.openjdk.jmh.annotations
import org.openjdk.jmh.annotations.Scope

/**
  * An entry that can be posted to the list
  */
@annotations.State(Scope.Benchmark)
class Entry() {
  val entry = new AtomicReference[State](new Initialized())
}
