package org.brandonscala

import org.openjdk.jmh.annotations
import org.openjdk.jmh.annotations._

import scala.util.Random

/**
  * Code Monkeys who observe and delete tasks from the list
  */
@annotations.State(Scope.Benchmark)
class CodeMonkey(s: Server) extends Thread{

  /**
    * Run method that actually runs the mokeys
    */
  override def run(): Unit = {
    val server = s
    val random = new Random()
    var counter: Int = 0
    while(counter < 500) {
      counter += 1
      if(server.jobSize().equals(0))
        Thread.sleep(random.nextInt(10))
      else {
        val list = server.examine()
        val numberChoice = random.nextInt(15)
        var choiceMade: Boolean = false
        var entryChoice: Entry = null
        var entryIndex: Int = 0
        var counter: Int = 0
        list.foreach(f => {
          if(random.nextInt(15).equals(numberChoice) && !choiceMade) {
            entryChoice = f._2
            entryIndex = f._1
            choiceMade = true
          }
          counter += 1
        })
        if(choiceMade) {
          server.delete(entryChoice, entryIndex)
        }
      }
    }
  }
}

