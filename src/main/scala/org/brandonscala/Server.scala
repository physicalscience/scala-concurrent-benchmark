package org.brandonscala

import org.openjdk.jmh.annotations
import org.openjdk.jmh.annotations.Scope

/**
  * The Server for the list
  */
@annotations.State(Scope.Thread)
class Server(jL: JobsList[Entry]) {
  private val jobList = jL
  private var jobCounter = 0

  /**
    * Adds an entry to the joblist
    */
  def add () = {
    val newEntry = new Entry()
    jobList.add(jobCounter, newEntry)
    jobCounter += 1
  }

  /**
    * Examines the list
    * @return returns the size of the list
    */
  def examine(): List[(Integer, Entry)] = jobList.examine()

  /**
    * Deletes an entry from the list
    * @param entry takes in the entry being looked for
    * @param index the index of the entry to be deleted
    * @return returns a boolean
    */
  def delete(entry: Entry, index: Int): Boolean = {
    val look = entry.entry.get()
    look match {
      case i: Initialized =>
        if(entry.entry.compareAndSet(look, new Deleting)) jobList.delete(index)
        else delete(entry, index)
      case d: Deleting =>
        false
    }
  }

  /**
    * Gets an entry from the job list
    * @param index the index of the job requested
    * @return returns an optional of Entry
    */
  def getEntry(index: Int): Option[Entry] = {
    val entry = jobList.grab(index)
    entry match {
      case Some(o) =>
        o.entry.get() match {
          case i: Initialized =>
            Some(o)
          case d: Deleting =>
            None
          case _ =>
            None
        }
      case None =>
        None
    }
  }

  /**
    * Gets the size of the joblist
    * @return returns a int
    */
  def jobSize(): Int = {
    jobList.size()
  }
}
