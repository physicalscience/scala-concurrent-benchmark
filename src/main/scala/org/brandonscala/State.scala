package org.brandonscala

import org.openjdk.jmh.annotations
import org.openjdk.jmh.annotations.Scope

/**
  * States of the jobs posted to the list
  */
@annotations.State(Scope.Benchmark)
sealed trait State

/**
  * The initialized state
  */
@annotations.State(Scope.Benchmark)
class Initialized extends State

/**
  * The deleted state
  */
@annotations.State(Scope.Benchmark)
class Deleting extends State
