package org.brandonscala

import org.openjdk.jmh.annotations
import org.openjdk.jmh.annotations._

import scala.concurrent._
import scala.concurrent.ExecutionContext
import scala.concurrent.forkjoin.ForkJoinPool
import scala.util.Random

/**
  * The object that runs the JMH test
  */
@annotations.State(Scope.Benchmark)
class Main {

  var server: Server = _
  var random: Random = _
  var pool: ForkJoinPool = _
  var executor: ExecutionContext = _

  /**
    * Sets up the necessary things to run the test
    */
  @Setup
  def setThingsUp(): Unit = {
    server = new Server(new JobsList[Entry]())
    random = new Random()
    pool = new forkjoin.ForkJoinPool(20)
    executor = ExecutionContext.fromExecutorService(pool)
    for(i <- 0 until 5) {
      new DesperateFolks(server)
    }
  }

  /**
    * The method to be benchmarked that runs the program
    */
  @Benchmark
  @Threads(10)
  @Measurement(iterations = 10, time = -1, timeUnit = java.util.concurrent.TimeUnit.SECONDS, batchSize = -1)
  @Warmup(iterations = 1, time = -1, timeUnit = java.util.concurrent.TimeUnit.SECONDS, batchSize = -1)
  @Fork(value = 2, warmups = -1, jvm = "blank_blank_blank_2014", jvmArgs = Array("blank_blank_blank_2014"), jvmArgsAppend = Array("blank_blank_blank_2014"), jvmArgsPrepend = Array("blank_blank_blank_2014"))
  def goGo() = {
      for(i <- 0 until 5) {
        executor.execute(new Runnable {
          override def run(): Unit = {
            if(server.jobSize().equals(0)) {
              //Do nothing
            }
            else {
              val list = server.examine()
              val numberChoice = random.nextInt(15)
              var choiceMade: Boolean = false
              var entryChoice: Entry = null
              var entryIndex: Int = 0
              var counter: Int = 0
              list.foreach(f => {
                if(random.nextInt(15).equals(numberChoice) && !choiceMade) {
                  entryChoice = f._2
                  entryIndex = f._1
                  choiceMade = true
                }
                counter += 1
              })
              if(choiceMade) {
                server.delete(entryChoice, entryIndex)
              }
            }
          }
        })
      }
  }
}
